# __author__ = 'holger.fischer@hoonet.org'

## IMPORTS
import argparse

import sys
import logging
import subprocess
import pathlib
import json

## LOGGING
loglevel=logging.WARNING

logging.basicConfig(level=loglevel,
        format=
        '%(asctime)s.%(msecs)03d %(levelname)s '
        '%(module)s - %(funcName)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')

LOG = logging.getLogger()

## CONSTANTS
WORK_DIRS = [
        'rootfs',
        'image',
        'log',
        ]

## FUNCTIONS
def prepare_folder_structure(buildname, out_dir):
    """prepare all folders for debb build
    """
    for work_dir in WORK_DIRS:
        full_dir = out_dir + '/' + work_dir + '/' + buildname
        pathlib.Path(full_dir).mkdir(parents=True, exist_ok=True)
        LOG.debug('out_dir {} + '
                'work_dir {} + '
                'buildname{} = '
                'full_dir {}'.format(
                    out_dir,
                    work_dir,
                    buildname,
                    full_dir,))

def build(buildname, cfg_dir, out_dir, rootfsname, vmdb2_extra_opts):

    #$DEBB_EXTRA_OPTS
    buildcmd = [ 
            'vmdb2', 
            '--rootfs-tarball',
            out_dir + '/rootfs/' + rootfsname + '/' + rootfsname + '.tar.gz',
            '--output',
            out_dir + '/image/' + buildname + '/' + buildname + '.img', 
            '--log',
            out_dir + '/log/' + buildname + '/' + buildname + '.log', 
            cfg_dir + '/' + buildname + '/' + buildname + '.yaml', 
            ]

    buildcmd += vmdb2_extra_opts

    LOG.info('buildcmd {}'.format(buildcmd))

    #FIXME: sanity checks
    if buildcmd:
        try:
            subprocess.run(buildcmd)
        except Exception as e:
            LOG.exception('Unable to build image for'
                      'buildname {} '
                      'Error: {}'.format(
                      buildname, e))
            raise Exception
        else:
            LOG.debug('Successfully build image for '
                      'buildname {} '.format(
                      buildname))


## MAIN
def main():
    global loglevel

    parser = argparse.ArgumentParser(
        prog='debb',
        description='debb - Debian builder - '
                'build Debian images using vmdb2',
    )
    requiredParser = parser.add_argument_group('required arguments')


    requiredParser.add_argument('--buildname',
                        required=True,
                        dest='buildname',
                        help=('buildname of the build'))
    parser.add_argument('--cfg-dir',
                        dest='cfg_dir',
                        default='/app/cfg',
                        help='directory where all debb cfg files '
                        'are located; defaults to /app/cfg')
    parser.add_argument('--out-dir',
                        dest='out_dir',
                        default='/app/out',
                        help='directory where all debb out files '
                        'are located; defaults to /app/out')
    parser.add_argument('--rootfsname',
                        dest='rootfsname',
                        help='buildname where rootfs archive should '
                        'be taken from (must already exist, optional)')
    parser.add_argument('--vmdb2-extra-opts',
                        dest='vmdb2_extra_opts',
                        help='comma separated list of extra opts '
                        'for vmdb2 (optional)')
    parser.add_argument('--verbose',
                        dest='verbose',
                        action="store_true",
                        help='enable verbose output')
    parser.add_argument('--debug',
                        dest='debug',
                        action="store_true",
                        help='enable debug output')

    args = parser.parse_args()

    if args.verbose:
        loglevel=logging.INFO
    elif args.debug:
        loglevel=logging.NOTSET

    LOG.setLevel(loglevel)
    LOG.info('Loglevel: {}'.format(
        logging.getLevelName(LOG.getEffectiveLevel())))

    LOG.info('args:\n'
            '    args.buildname: {}\n'
            '    args.cfg_dir {}\n'
            '    args.out_dir {}\n'
            '    args.rootfsname: {}\n'
            '    args.vmdb2_extra_opts: {}\n'.format(
                args.buildname,
                args.cfg_dir,
                args.out_dir,
                args.rootfsname,
                args.vmdb2_extra_opts,
                ))

    if args.rootfsname:
        rootfsname = args.rootfsname
    else:
        rootfsname = args.buildname

    LOG.info('rootfsname: {}'.format(rootfsname))

    if args.vmdb2_extra_opts:
        vmdb2_extra_opts = args.vmdb2_extra_opts.split(',')
    else:
        vmdb2_extra_opts = []

    LOG.info('vmdb2_extra_opts: {}'.format(vmdb2_extra_opts))

    prepare_folder_structure(
            buildname = args.buildname, 
            out_dir = args.out_dir, 
            )

    build(
            buildname = args.buildname, 
            cfg_dir = args.cfg_dir, 
            out_dir = args.out_dir, 
            rootfsname = rootfsname, 
            vmdb2_extra_opts = vmdb2_extra_opts,
            )

if __name__ == '__main__':
    sys.exit(main())
